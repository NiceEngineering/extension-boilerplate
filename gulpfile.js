var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var pkg = require('./package.json');

// Set the banner content
var banner = ['/*!\n',
  ' * Start Extension - <%= pkg.title %> v<%= pkg.version %> \n',
  ' */\n',
  ''
].join('');

var buildConfig = {
  "basedir": "app/output/",
  "html": "app/*.html",
  "output-dst": "app/output"
}

var jsConfig = {
  "js-src": "app/js/*.js",
  "js-dst": "app/output/js",
  "js-ignore": "!app/js/*.min.js"
}

var styleConfig = {
  "main-name": "style.min.css",
  "main-ignore-name": "!app/css/style.min.css",
  "css-src": "app/css/*.css",
  "css-dst": "app/output/css",
  "css-min-src": "app/css/*.min.css",
  "css-min-ignore": "!app/css/*.min.css",
  "scss-src": "app/scss/*.scss",
}

// Compiles SCSS files from /scss into /css
const scss = (cb) => {
  return gulp.src( styleConfig["scss-src"] )
    .pipe(sass())
    .pipe(header(banner, {
      pkg: pkg
    }))
    .pipe(gulp.dest( styleConfig["css-dst"] ))
    .pipe(browserSync.reload({
      stream: true
    }));
}
exports.scss = scss;


// Minify compiled CSS
const minifyCss = (cb) => {
  gulp.src([ styleConfig["css-src"] , styleConfig["css-min-ignore"] ])
    .pipe(cleanCSS({
      compatibility: 'ie10'
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest( styleConfig["css-dst"] ))
    .pipe(browserSync.reload({
      stream: true
    }))
  cb();
}

exports.minifyCss = gulp.series(scss, minifyCss);

// Minify custom JS
const minifyJs = (cb) => {
  gulp.src([ jsConfig["js-src"] , jsConfig["js-ignore"] ])
    .pipe(uglify())
    .pipe(header(banner, {
      pkg: pkg
    }))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest( jsConfig["js-dst"] ))
    .pipe(browserSync.reload({
      stream: true
    }))
  cb();
}

exports.minifyJs = minifyJs;

const outputHtml = (cb) => {
  gulp.src(buildConfig.html)
    .pipe( gulp.dest( buildConfig["output-dst"]))
  cb();
}

exports.outputHtml = outputHtml;

// Default task
exports.default = gulp.parallel( minifyCss, minifyJs);

// Configure the browserSync task
const serv = (done) => {
    browserSync.init({
      server: {
        baseDir: buildConfig.basedir
      },
    })

    done();
}

exports.browserSync = serv;

const watch = (done) => {
  gulp.watch(styleConfig["scss-src"], scss);
  gulp.watch(jsConfig["js-src"], minifyJs);

  gulp.watch(buildConfig.html, gulp.series(outputHtml, browserSync.reload) );
  gulp.watch(jsConfig["js-src"], browserSync.reload);

  done();
}
exports.watch = watch;

exports.dev = gulp.series(
  minifyCss,
  minifyJs,
  browserSync,
  watch
)

exports.devRig = gulp.series(
  minifyCss,
  minifyJs
)

// Dev task with browserSync
// gulp.task('dev', ['browserSync', 'sass', 'minify-css', 'minify-js'], function() {
//   gulp.watch('app/scss/*.scss', ['sass']);
//   gulp.watch('app/css/*.css', ['minify-css', 'concat-css']);
//   gulp.watch('app/js/*.js', ['minify-js']);
//   // Reloads the browser whenever HTML or JS files change
//   gulp.watch('app/*.html', browserSync.reload);
//   gulp.watch('app/js/*.js', browserSync.reload);
// });

// Dev task with browserSync
// gulp.task('dev-rig', ['sass', 'minify-css', 'minify-js'], function() {
//   gulp.watch('app/scss/*.scss', ['sass', 'minify-css']);
//   gulp.watch(['app/js/**/*.js', '!app/js/**/*.min.js'], ['minify-js']);
//   // gulp.watch('app/css/*.css', ['minify-css', 'concat-css']);
// });