

## Installation
Git Clone the repository. Open your terminal and navigate to the folder. Assuming you alread have 'npm' installed(if not, head here https://nodejs.org/en/ and install normally), enter 'npm install' to download/install dependencies. Enjoy your dev-ing

## Running locally
In your terminal, at the root folder, type 'gulp' to run the tasks for minifying, and converting scss to css. Type 'gulp dev' to run the local server. Open chrome and type up 'localhost:3000/viewer.html' to see the viewer page. Don't forget ''.html' and port number when trying to access the pages or you can get something along the lines of 'Cannot GET /viewer'

# Everything Below this Line was from original Boilerplate. I've, since, changed it up to allow us to use scss, npm, and gulp.

# Twitch Extensions Boilerplate

The Twitch Extensions Boilerplate acts as a simple starting point to create your Extension, as well as a simple method using Docker to locally serve your Extension for testing and development.

## Dependencies

You will need:
 * [docker](https://docs.docker.com/engine/installation/)
 * [docker-compose](https://docs.docker.com/compose/install/)

## Generate self-signed certs
```bash
cd certs
./generate_local_ssl.sh
    # Requires a sudo password so that the cert can be installed on the root keychain
    # If this install fails, see the README in ./certs for manual override.
```

## To start the Extensions Boilerplate service
```bash
docker-compose up --build
```

## Further documentation

Please consult the [Twitch Extensions documentation on the Twitch developer site](https://dev.twitch.tv/docs/extensions)
